#include <stdio.h>

extern void Register(void (*pFunc)());

static void PrintFunc()
{
        printf("Hello from lib\n");
}

struct CRegistrator
{
        CRegistrator() {
                Register(&PrintFunc);
        }
};

static CRegistrator g_Registrator;
rm -f *.o *.a a.out

g++ -c -o Lib.o Lib.cpp
ar r libLib.a Lib.o
g++ -c -o Main.o Main.cpp
g++ Main.o -Wl,--whole-archive libLib.a -Wl,--no-whole-archive -o a.out
./a.out
#include <iostream>

static void (*g_pFunc)() = 0;

void Register(void (*pFunc)())
{
        g_pFunc = pFunc;
}

int main()
{
        if (g_pFunc)
                g_pFunc();
        return 0;
}